# Maze

This is a project created to test the latest version of Delphi. I plan to generate mazes to save them in files. To read mazes from files. To move in these mazes and to find the exit automatically.

##  MazeGenerator

Console project.

Allows you to generate a labyrinth. I used the python algorithm on [this page](http://rosettacode.org/wiki/Maze_generation) for the generation.

[To see](http://rosettacode.org/wiki/Maze_solving#Delphi)

The files are saved in a sub-folder of the exe.

## MazeReader

Console project.

Read a text file to load a maze. Load file `C:\temp\maze.txt`.

Load an object corresponding to the labyrinth.

## MazeDraw

FMX project.

Draw maze in file: `C:\temp\maze.txt`.

## MazeSolve

Draw maze and solve it. Go from the top left box to the bottom right box (from file: `C:\temp\maze.txt`).

## MazeMove

Move in maze from file: `C:\temp\maze.txt`.
