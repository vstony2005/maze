unit UMazeGenerate;

{$DEFINE BLOCK}

interface

uses
  Math,
  System.SysUtils,
  System.Generics.Collections,
  System.Generics.Defaults;

type
  TGenericListHelper = class
    class procedure Shuffle<T>(const listOfT : TList<T>);
    class procedure UnSort<T>(const listOfT : TList<T>);
  end;

  TMaze = class
  private
    FHeight: Integer;
    FWidth: Integer;
    FVis: array of array of Boolean;  // visit
    FVer: array of array of string;   // murs verticaux
    FHor: array of array of string;   // murs horizontaux
    constructor Create(AHeight, AWidth: Integer);
    procedure Walk(ax, ay: Integer);
    procedure PrintConsole;
    function GetString: string;
    procedure SaveToFile;
  public
    class procedure Make(h: Integer = 5; w: Integer = 5);
  end;

implementation

uses
  Classes, System.IOUtils, System.Types;

{ TGenericListHelper }

class procedure TGenericListHelper.Shuffle<T>(const listOfT: TList<T>);
//randomize positions by swapping the position of two elements randomly
var
  randomIndex: integer;
  cnt: integer;
begin
  for cnt := 0 to listOfT.Count-1 do
  begin
    randomIndex := Random(listOfT.Count - cnt);
    listOfT.Exchange(cnt, cnt + randomIndex);
  end;
end;

class procedure TGenericListHelper.UnSort<T>(const listOfT: TList<T>);
//randomize positions by unsorting
begin
  listOfT.Sort(TComparer<T>.Construct(
    function(const Left, Right : T) : integer
    begin
      result := -1 + Random(3);
    end
  ));
end;

{ TMaze }

class procedure TMaze.Make(h, w: Integer);
var
  maze: TMaze;
  hr, wr: Integer;
begin
  maze := TMaze.Create(Max(5,h), Max(5,w));
  hr := Random(h);
  wr := Random(w);
  maze.Walk(wr, hr);
  maze.PrintConsole;

  maze.SaveToFile;

  Writeln('end');

  maze.Free;
end;

constructor TMaze.Create(AHeight, AWidth: Integer);
(*
sample for 3x3

  vis
  [[0, 0, 0, 1],
   [0, 0, 0, 1],
   [0, 0, 0, 1],
   [1, 1, 1, 1]]
  ver
  [['|  ', '|  ', '|  ', '|'],
   ['|  ', '|  ', '|  ', '|'],
   ['|  ', '|  ', '|  ', '|'],
   []]
  hor
  [['+--', '+--', '+--', '+'],
   ['+--', '+--', '+--', '+'],
   ['+--', '+--', '+--', '+'],
   ['+--', '+--', '+--', '+']]
*)
const
  {$IFDEF BLOCK}
  MUR_VER: string = '# ';
  MUR_HOR: string = '##';
  MUR_VER_LAST: string = '#';
  MUR_HOR_LAST: string = '#';
  {$ELSE}
  MUR_VER: string = '|  ';
  MUR_HOR: string = '+--';
  MUR_VER_LAST: string = '|';
  MUR_HOR_LAST: string = '+';
  {$ENDIF}
var
  i, j: Integer;
begin
  FHeight := AHeight;
  FWidth := AWidth;

  SetLength(FVis, FHeight+1, FWidth+1);
  SetLength(FVer, FHeight+1, FWidth+1);
  SetLength(FHor, FHeight+1, FWidth+1);

  // init vis
  for i := 0 to FHeight-1 do
    FVis[i][FWidth] := True;

  for i := 0 to FWidth do
    FVis[FHeight][i] := True;

  // init ver / hor
  for i:=0 to FHeight do
    for j:=0 to FWidth do
    begin
      if (j = FWidth) then
      begin
        if (i < FHeight) then
          FVer[i][j] := MUR_VER_LAST;
        FHor[i][j] := MUR_HOR_LAST;
      end
      else
      begin
        if (i < FHeight) then
          FVer[i][j] := MUR_VER;
        FHor[i][j] := MUR_HOR;
      end;
    end;
end;

procedure TMaze.Walk(ax, ay: Integer);
const
  {$IFDEF BLOCK}
  MUR_HOR_VIDE: string = '# ';
  MUR_VER_VIDE: string = '  ';
  {$ELSE}
  MUR_HOR_VIDE: string = '+  ';
  MUR_VER_VIDE: string = '   ';
  {$ENDIF}
var
  points: TList<TPoint>;
  i: Integer;
  l: TList<Integer>;
  x,y: Integer;
begin
  FVis[ay][ax] := True;

  points := TList<TPoint>.Create;
  points.Add(Point(ax-1, ay));
//  points.Add(Point(ax-1, ay));
//  points.Add(Point(ax-1, ay));
  points.Add(Point(ax, ay-1));
  points.Add(Point(ax+1, ay));
//  points.Add(Point(ax+1, ay));
//  points.Add(Point(ax+1, ay));
  points.Add(Point(ax, ay+1));

  TGenericListHelper.Shuffle<TPoint>(points);

  try
    for i:=0 to 3 do
    begin
      x := points[i].x;
      y := points[i].y;

      if (x < 0) or (y < 0) or (FVis[y][x]) then
        Continue;

      if (ax = x) then FHor[Max(ay,y)][ax] := MUR_HOR_VIDE;
      if (ay = y) then FVer[ay][Max(ax,x)] := MUR_VER_VIDE;

      Walk(x, y);
    end;
  except
    on e: Exception do
      Writeln(e.Message);
  end;
  points.Free;
end;

procedure TMaze.PrintConsole;
var
  i, j: Integer;
begin
  for i := 0 to FHeight do
  begin
    for j := 0 to FWidth do
      Write(FHor[i][j]);
    Writeln;

    for j := 0 to FWidth do
      Write(FVer[i][j]);
    Writeln;
  end;
end;

procedure TMaze.SaveToFile;

  procedure StringToStream(const Str: string; const Stm: Classes.TStream);
  var
    SS: Classes.TStringStream;
  begin
    SS := Classes.TStringStream.Create(Str);
    try
      Stm.CopyFrom(SS, Length(Str));
    finally
      SS.Free;
    end;
  end;

  procedure StringToFile(const Str, FileName: string);
  var
    FS: Classes.TFileStream;
  begin
    FS := Classes.TFileStream.Create(FileName, Classes.fmCreate);
    try
      StringToStream(Str, FS);
    finally
      FS.Free;
    end;
  end;

var
  dir: string;
begin
  dir := IncludeTrailingPathDelimiter(TPath.Combine(GetCurrentDir, 'maze'));
  if (not DirectoryExists(dir)) then
    TDirectory.CreateDirectory(dir);

  StringToFile(
    GetString,
    Format('%smaze_%s.txt',
           [dir,
            FormatDateTime('yyyymmdd_hhnnss-zzz', Now)]));
end;

function TMaze.GetString: string;
var
  i, j: Integer;
begin
  Result := '';
  for i := 0 to FHeight do
  begin
    for j := 0 to FWidth do
      Result := Result + FHor[i][j];
    Result := Result + sLineBreak;
    for j := 0 to FWidth do
      Result := Result + FVer[i][j];
    Result := Result + sLineBreak;
  end;
end;

initialization
  Randomize;

end.
