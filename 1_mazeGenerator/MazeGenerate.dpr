program MazeGenerate;

{$APPTYPE CONSOLE}

{$R *.res}

uses
  System.SysUtils,
  UMazeGenerate in 'UMazeGenerate.pas';

begin
  try
    Randomize;
    for var i:=0 to 20 do
      TMaze.Make(Random(25),Random(25));

    Readln;
  except
    on E: Exception do
      Writeln(E.ClassName, ': ', E.Message);
  end;
end.
