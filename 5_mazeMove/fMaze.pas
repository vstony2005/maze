unit fMaze;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes,
  System.Variants,
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  FMX.Ani, System.ImageList, FMX.ImgList, FMX.Objects, FMX.Layouts, UMaze;

type
{$SCOPEDENUMS ON}
  TMoveDir = (None, Left, Up, Right, Down);
{$SCOPEDENUMS OFF}
  TMoveDirEvent = procedure(Sender: TObject; MoveDir: TMoveDir) of object;

  TFrmMaze = class(TFrame)
    ScrollBox1: TScrollBox;
    recFond: TRectangle;
    recCaract: TRectangle;
    imgs: TImageList;
    anim: TFloatAnimation;
    procedure animFinish(Sender: TObject);
  private
    FImgMaze: TBitmap;
    FMaze: TMaze;
    FPosition: TPoint;
    FOnMoveChange: TMoveDirEvent;
    function LoadImages: Boolean;
    function AddImage(aBitmap: TBitmap; const name: string): Integer;
    function CanMove(aDir: TMoveDir): Boolean;
    procedure DrawCase(bmp: TBitmap; const name: string; const pos: TRect);
    procedure DrawMaze;
    function GetImageSize: Integer;
    function GetName(const aCase: TCase; l, c: Integer): string;
    property ImageSize: Integer read GetImageSize;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure MoveCaracter(aDir: TMoveDir);
  published
    property OnMoveChange: TMoveDirEvent read FOnMoveChange write FOnMoveChange;
  end;

implementation

{$R *.fmx}

uses
  FMX.MultiResBitmap;

{ TFrmMaze }

constructor TFrmMaze.Create(AOwner: TComponent);
begin
  inherited;

  FMaze := nil;
  FImgMaze := nil;
  if (LoadImages) then
    DrawMaze
end;

destructor TFrmMaze.Destroy;
begin
  if Assigned(FImgMaze) then
    FImgMaze.Free;
  if Assigned(FMaze) then
    FMaze.Free;

  inherited;
end;

function TFrmMaze.GetImageSize: Integer;
const
  SIZE_IMG = 16;
begin
  Result := SIZE_IMG;
end;

function TFrmMaze.AddImage(aBitmap: TBitmap; const name: string): Integer;
const
  SCALE = 1;
var
  srcItm: TCustomSourceItem;
  bmpItm: TCustomBitmapItem;
  dest: TCustomDestinationItem;
  layer: TLayer;
begin
  Result := -1;
  if (aBitmap.Width = 0) or (aBitmap.Height = 0) then
    Exit;

  // add source bitmap
  srcItm := imgs.Source.Add;
  srcItm.MultiResBitmap.TransparentColor := TColorRec.Fuchsia;
  srcItm.MultiResBitmap.SizeKind := TSizeKind.Source;
  srcItm.MultiResBitmap.Width := Round(aBitmap.Width / SCALE);
  srcItm.MultiResBitmap.Height := Round(aBitmap.Height / SCALE);
  srcItm.name := name;
  bmpItm := srcItm.MultiResBitmap.ItemByScale(SCALE, True, True);

  if not Assigned(bmpItm) then
  begin
    bmpItm := srcItm.MultiResBitmap.Add;
    bmpItm.SCALE := SCALE;
  end;

  bmpItm.Bitmap.Assign(aBitmap);

  dest := imgs.Destination.Add;
  layer := dest.Layers.Add;
  layer.SourceRect.Rect := TRectF.Create(TPoint.Zero,
    srcItm.MultiResBitmap.Width, srcItm.MultiResBitmap.Height);
  layer.name := srcItm.name;

  Result := dest.Index;
end;

function TFrmMaze.LoadImages: Boolean;
const
  PACK_FILE = '..\..\..\imgs\nb2.png';
var
  i: Integer;
  pack: TBitmap;
  img: TBitmap;
  rec: TRect;
begin
  Result := False;
  if FileExists(PACK_FILE) then
  begin
    pack := TBitmap.Create;
    try
      pack.LoadFromFile(PACK_FILE);

      for i := 0 to 24 do
      begin
        rec := Rect(i * ImageSize, 0, i * ImageSize + ImageSize, ImageSize);
        img := TBitmap.Create(ImageSize, ImageSize);
        img.CopyFromBitmap(pack, rec, 0, 0);
        AddImage(img, i.ToString);
        img.Free;
      end;

      Result := True;
    finally
      pack.Free;
    end;
  end;
end;

procedure TFrmMaze.MoveCaracter(aDir: TMoveDir);
begin
  if (anim.Enabled) then
    Exit;

  if (not CanMove(aDir)) then
    Exit;

  case aDir of
    TMoveDir.Left, TMoveDir.Right:
      begin
        anim.PropertyName := 'Position.X';
        anim.StartValue := recCaract.Position.X;
      end;
    TMoveDir.Up, TMoveDir.Down:
      begin
        anim.PropertyName := 'Position.Y';
        anim.StartValue := recCaract.Position.Y;
      end;
  else
    Exit;
  end;

  case aDir of
    TMoveDir.Right, TMoveDir.Down:
      anim.StopValue := anim.StartValue + ImageSize;
    TMoveDir.Left, TMoveDir.Up:
      anim.StopValue := anim.StartValue - ImageSize;
  else
    Exit;
  end;

  case aDir of
    TMoveDir.Left:
      Dec(FPosition.X);
    TMoveDir.Up:
      Dec(FPosition.Y);
    TMoveDir.Right:
      Inc(FPosition.X);
    TMoveDir.Down:
      Inc(FPosition.Y);
  end;

  if Assigned(FOnMoveChange) then
    FOnMoveChange(Self, aDir);

  anim.Start;
end;

procedure TFrmMaze.DrawMaze;
var
  l, c: Integer;
  bmp: TBitmap;
begin
  if Assigned(FMaze) then
    FMaze.Free;
  FMaze := TMaze.Create;
  if Assigned(FImgMaze) then
    FImgMaze.Free;

  FImgMaze := TBitmap.Create;
  try
    FMaze.LoadFile('C:\temp\maze.txt');

    FImgMaze.SetSize(FMaze.NbCol * ImageSize, FMaze.NbLin * ImageSize);

    // draw the maze
    for l := 0 to FMaze.NbLin - 1 do
    begin
      for c := 0 to FMaze.NbCol - 1 do
      begin
        DrawCase(FImgMaze, GetName(FMaze.Cases[l, c], l, c),
          Rect(ImageSize * c, ImageSize * l, ImageSize * c + ImageSize,
          ImageSize * l + ImageSize));
      end;
    end;

    // draw finish
    DrawCase(FImgMaze, '22', Rect(ImageSize * (FMaze.NbCol - 2),
      ImageSize * (FMaze.NbLin - 2), ImageSize * (FMaze.NbCol - 2) + ImageSize,
      ImageSize * (FMaze.NbLin - 2) + ImageSize));

    recFond.Size.Height := FMaze.NbLin * ImageSize;
    recFond.Size.Width := FMaze.NbCol * ImageSize;
    recFond.Fill.Bitmap.Bitmap.Assign(FImgMaze);
  finally
  end;

  // draw caracter
  bmp := TBitmap.Create(ImageSize, ImageSize);
  try
    DrawCase(bmp, '23', Rect(0, 0, ImageSize, ImageSize));

    recCaract.Position.X := ImageSize;
    recCaract.Position.Y := ImageSize;
    recCaract.Size.Height := ImageSize;
    recCaract.Size.Width := ImageSize;
    recCaract.Fill.Bitmap.Bitmap.Assign(bmp);
    FPosition := Point(1, 1);
  finally
    bmp.Free;
  end;
end;

procedure TFrmMaze.DrawCase(bmp: TBitmap; const name: string; const pos: TRect);
const
  SCALE = 1;
var
  bmpItm: TCustomBitmapItem;

  bmpTmp: TBitmap;

  Size: TSize;
  src: TRectF;
begin
  imgs.BitmapItemByName(name, bmpItm, Size);

  bmpTmp := bmpItm.Bitmap;
  src := TRectF.Create(0, 0, bmpTmp.Width, bmpTmp.Height);

  bmp.Canvas.BeginScene;
  bmp.Canvas.DrawBitmap(bmpTmp, src, pos, 1);
  bmp.Canvas.EndScene;
end;

procedure TFrmMaze.animFinish(Sender: TObject);
begin
  anim.Enabled := False;
  if Assigned(FOnMoveChange) then
    FOnMoveChange(Self, TMoveDir.None);
end;

function TFrmMaze.CanMove(aDir: TMoveDir): Boolean;
begin
  case aDir of
    TMoveDir.Left:
      Result := not FMaze.Cases[FPosition.Y, FPosition.X].WallLeft;
    TMoveDir.Up:
      Result := not FMaze.Cases[FPosition.Y, FPosition.X].WallUp;
    TMoveDir.Right:
      Result := not FMaze.Cases[FPosition.Y, FPosition.X].WallRight;
    TMoveDir.Down:
      Result := not FMaze.Cases[FPosition.Y, FPosition.X].WallDown;
  else
    Result := False;
  end;
end;

function TFrmMaze.GetName(const aCase: TCase; l, c: Integer): string;
var
  idx: Integer;
begin
  idx := -1;
  try
    if not(aCase.IsWall) then
      idx := 16
    else
      idx := StrToInt('$' + aCase.Code);
  except
    ShowMessageFmt('%s [%s] (%d-%d)', [aCase.CaseToString, aCase.Code, l, c]);
  end;

  case idx of
    0: // empty
      Result := '0';
    1: // left
      Result := '1';
    2: // up
      Result := '4';
    3: // left+up
      Result := '9';
    4: // right
      Result := '2';
    5: // right+left
      Result := '5';
    6: // up+right
      Result := '8';
    7: // left+up+right
      Result := '13';
    8: // down
      Result := '3';
    9: // left+down
      Result := '7';
    10: // up+down
      Result := '6';
    11: // left+up+down
      Result := '14';
    12: // right+down
      Result := '10';
    13: // left+right+down
      Result := '11';
    14: // up+right+down
      Result := '12';
    15: // left+up+right+down
      Result := '15';
  else
    Result := '17';
  end;
end;

end.
