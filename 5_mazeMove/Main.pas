unit Main;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes,
  System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, fMaze,
  System.ImageList, FMX.ImgList, FMX.SVGIconImageList, FMX.Layouts;

type
  TfrmMain = class(TForm)
    FrmMaze1: TFrmMaze;
    SVGIconImageList1: TSVGIconImageList;
    Glyph1: TGlyph;
    Layout1: TLayout;
    Layout2: TLayout;
    procedure FormCreate(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; var KeyChar: Char;
      Shift: TShiftState);
  private
    procedure MoveChange(Sender: TObject; aDir: TMoveDir);
  public
  end;

var
  frmMain: TfrmMain;

implementation

{$R *.fmx}

procedure TfrmMain.FormCreate(Sender: TObject);
begin
  FrmMaze1.OnMoveChange := MoveChange;
end;

procedure TfrmMain.FormKeyDown(Sender: TObject; var Key: Word;
  var KeyChar: Char; Shift: TShiftState);
begin
  case Key of
    vkUp:
      FrmMaze1.MoveCaracter(TMoveDir.Up);
    vkRight:
      FrmMaze1.MoveCaracter(TMoveDir.Right);
    vkDown:
      FrmMaze1.MoveCaracter(TMoveDir.Down);
    vkLeft:
      FrmMaze1.MoveCaracter(TMoveDir.Left);
  end;
end;

procedure TfrmMain.MoveChange(Sender: TObject; aDir: TMoveDir);
begin
  case aDir of
    TMoveDir.Left:
      Glyph1.ImageIndex := 0;
    TMoveDir.Up:
      Glyph1.ImageIndex := 1;
    TMoveDir.Right:
      Glyph1.ImageIndex := 2;
    TMoveDir.Down:
      Glyph1.ImageIndex := 3;
  else
    Glyph1.ImageIndex := -1;
  end;
end;

end.
