unit Main;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes,
  System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  System.ImageList, FMX.ImgList, FMX.Layouts, UMaze;

type
  TForm1 = class(TForm)
    imgs: TImageList;
    VertScrollBox1: TVertScrollBox;
    procedure FormCreate(Sender: TObject);
  private
    function LoadImages: Boolean;
    function AddImage(aBitmap: TBitmap; const name: string): Integer;
    procedure DrawImage;
    procedure DrawCase(bmp: TBitmap; const name: string; const pos: TRect);
    procedure DrawMaze;
    function GetImageSize: Integer;
    function GetName(const aCase: TCase; l, c: Integer): string;
    property ImageSize: Integer read GetImageSize;
  public
  end;

var
  Form1: TForm1;

implementation

{$R *.fmx}

uses
  FMX.MultiResBitmap, FMX.Objects;

procedure TForm1.FormCreate(Sender: TObject);
begin
  if (LoadImages) then
    // DrawImage
    DrawMaze
  else;
end;

function TForm1.GetImageSize: Integer;
const
  SIZE_IMG = 16;
begin
  Result := SIZE_IMG;
end;

function TForm1.AddImage(aBitmap: TBitmap; const name: string): Integer;
const
  SCALE = 1;
var
  srcItm: TCustomSourceItem;
  bmpItm: TCustomBitmapItem;
  dest: TCustomDestinationItem;
  layer: TLayer;
begin
  Result := -1;
  if (aBitmap.Width = 0) or (aBitmap.Height = 0) then
    Exit;

  // add source bitmap
  srcItm := imgs.Source.Add;
  srcItm.MultiResBitmap.TransparentColor := TColorRec.Fuchsia;
  srcItm.MultiResBitmap.SizeKind := TSizeKind.Source;
  srcItm.MultiResBitmap.Width := Round(aBitmap.Width / SCALE);
  srcItm.MultiResBitmap.Height := Round(aBitmap.Height / SCALE);
  srcItm.name := name;
  bmpItm := srcItm.MultiResBitmap.ItemByScale(SCALE, True, True);

  if not Assigned(bmpItm) then
  begin
    bmpItm := srcItm.MultiResBitmap.Add;
    bmpItm.SCALE := SCALE;
  end;

  bmpItm.Bitmap.Assign(aBitmap);

  dest := imgs.Destination.Add;
  layer := dest.Layers.Add;
  layer.SourceRect.Rect := TRectF.Create(TPoint.Zero,
    srcItm.MultiResBitmap.Width, srcItm.MultiResBitmap.Height);
  layer.name := srcItm.name;

  Result := dest.Index;
end;

function TForm1.LoadImages: Boolean;
const
  PACK_FILE = '..\..\..\imgs\nb2.png';
var
  i: Integer;
  pack: TBitmap;
  img: TBitmap;
  rec: TRect;
begin
  Result := False;
  if FileExists(PACK_FILE) then
  begin
    pack := TBitmap.Create;
    try
      pack.LoadFromFile(PACK_FILE);

      for i := 0 to 24 do
      begin
        rec := Rect(i * ImageSize, 0, i * ImageSize + ImageSize, ImageSize);
        img := TBitmap.Create(ImageSize, ImageSize);
        img.CopyFromBitmap(pack, rec, 0, 0);
        AddImage(img, i.ToString);
        img.Free;
      end;

      Result := True;
    finally
      pack.Free;
    end;
  end;
end;

procedure TForm1.DrawCase(bmp: TBitmap; const name: string; const pos: TRect);
const
  SCALE = 1;
var
  bmpItm: TCustomBitmapItem;

  bmpTmp: TBitmap;

  Size: TSize;
  src: TRectF;
begin
  imgs.BitmapItemByName(name, bmpItm, Size);

  bmpTmp := bmpItm.Bitmap;
  src := TRectF.Create(0, 0, bmpTmp.Width, bmpTmp.Height);

  bmp.Canvas.BeginScene;
  bmp.Canvas.DrawBitmap(bmpTmp, src, pos, 1);
  bmp.Canvas.EndScene;
end;

procedure TForm1.DrawImage;
// for test only
var
  i: Integer;
  img1: TBitmap;
  rec: TRectangle;
begin
  img1 := TBitmap.Create(ImageSize * 10, ImageSize);
  for i := 0 to 10 do
  begin
    DrawCase(img1, '0', Rect(i * ImageSize, 0, i * ImageSize + ImageSize,
      ImageSize));
  end;

  rec := TRectangle.Create(Self);
  rec.Parent := VertScrollBox1;
  rec.Position.X := 10;
  rec.Position.Y := 10;
  rec.Size.Height := ImageSize;
  rec.Size.Width := ImageSize * 10;
  rec.Fill.Kind := TBrushKind.Bitmap;
  rec.Fill.Bitmap.WrapMode := TWrapMode.TileStretch;
  rec.Stroke.Kind := TBrushKind.None;

  rec.Fill.Bitmap.Bitmap.Assign(img1);
  img1.Free;

  // -----------------------------------------------

  img1 := TBitmap.Create(ImageSize * 10, ImageSize);
  for i := 0 to 10 do
  begin
    DrawCase(img1, '23', Rect(i * ImageSize, 0, i * ImageSize + ImageSize,
      ImageSize));
  end;

  rec := TRectangle.Create(Self);
  rec.Parent := VertScrollBox1;
  rec.Position.X := 10;
  rec.Position.Y := 36;
  rec.Size.Height := ImageSize;
  rec.Size.Width := ImageSize * 10;
  rec.Fill.Kind := TBrushKind.Bitmap;
  rec.Fill.Bitmap.WrapMode := TWrapMode.TileStretch;
  rec.Stroke.Kind := TBrushKind.None;

  rec.Fill.Bitmap.Bitmap.Assign(img1);
  img1.Free;

  // -----------------------------------------------

  img1 := TBitmap.Create(ImageSize * 10, ImageSize);
  for i := 0 to 10 do
  begin
    DrawCase(img1, '17', Rect(i * ImageSize, 0, i * ImageSize + ImageSize,
      ImageSize));
  end;

  rec := TRectangle.Create(Self);
  rec.Parent := VertScrollBox1;
  rec.Position.X := 10;
  rec.Position.Y := 62;
  rec.Size.Height := ImageSize;
  rec.Size.Width := ImageSize * 10;
  rec.Fill.Kind := TBrushKind.Bitmap;
  rec.Fill.Bitmap.WrapMode := TWrapMode.TileStretch;
  rec.Stroke.Kind := TBrushKind.None;

  rec.Fill.Bitmap.Bitmap.Assign(img1);
  img1.Free;
end;

procedure TForm1.DrawMaze;
var
  aMaze: TMaze;
  l, c: Integer;

  bmp: TBitmap;
  rec: TRectangle;
begin
  aMaze := TMaze.Create;
  bmp := TBitmap.Create;
  try
    aMaze.LoadFile('C:\temp\maze.txt');
    bmp.SetSize(aMaze.NbCol * ImageSize, aMaze.NbLin * ImageSize);

    for l := 0 to aMaze.NbLin - 1 do
    begin
      for c := 0 to aMaze.NbCol - 1 do
      begin
        DrawCase(bmp, GetName(aMaze.Cases[l, c], l, c),
          Rect(ImageSize * c, ImageSize * l, ImageSize * c + ImageSize,
          ImageSize * l + ImageSize));
      end;
    end;

    rec := TRectangle.Create(Self);
    rec.Parent := VertScrollBox1;
    rec.Position.X := 10;
    rec.Position.Y := 10;
    rec.Size.Height := aMaze.NbLin * ImageSize;
    rec.Size.Width := aMaze.NbCol * ImageSize;
    rec.Fill.Kind := TBrushKind.Bitmap;
    rec.Fill.Bitmap.WrapMode := TWrapMode.TileStretch;
    rec.Stroke.Kind := TBrushKind.None;

    rec.Fill.Bitmap.Bitmap.Assign(bmp);
  finally
    bmp.Free;
    aMaze.Free;
  end;
end;

function TForm1.GetName(const aCase: TCase; l, c: Integer): string;
(*

  0 empty
  1 left
  2

*)
var
  idx: Integer;
begin
  idx := -1;
  try
    if not(aCase.IsWall) then
      idx := 16
    else
      idx := StrToInt('$' + aCase.Code);
  except
    ShowMessageFmt('%s [%s] (%d-%d)', [aCase.CaseToString, aCase.Code, l, c]);
  end;

  case idx of
    0: // empty
      Result := '0';
    1: // left
      Result := '1';
    2: // up
      Result := '4';
    3: // left+up
      Result := '9';
    4: // right
      Result := '2';
    5: // right+left
      Result := '5';
    6: // up+right
      Result := '8';
    7: // left+up+right
      Result := '13';
    8: // down
      Result := '3';
    9: // left+down
      Result := '7';
    10: // up+down
      Result := '6';
    11: // left+up+down
      Result := '14';
    12: // right+down
      Result := '10';
    13: // left+right+down
      Result := '11';
    14: // up+right+down
      Result := '12';
    15: // left+up+right+down
      Result := '15';
  else
    Result := '17';
  end;
end;

end.
