unit UMaze;

interface

uses
  System.SysUtils, Classes;

type
  TCase = record
    IsWall: Boolean;
    WallUp: Boolean;
    WallRight: Boolean;
    WallDown: Boolean;
    WallLeft: Boolean;
    Code: Char;
    function CaseToString: string;
  end;

  TMaze = class
  strict private
  private
    FNbLin: Integer;
    FNbCol: Integer;
    FCases: array of TCase;
    procedure LoadMaze(const lst: TStringList);
    function GetCase(l, c: Integer): TCase;
    procedure SetCaseIsWall(l, c: Integer; const Value: Boolean);
    procedure SetCaseWallDown(l, c: Integer; const Value: Boolean);
    procedure SetCaseWallLeft(l, c: Integer; const Value: Boolean);
    procedure SetCaseWallRight(l, c: Integer; const Value: Boolean);
    procedure SetCaseWallUp(l, c: Integer; const Value: Boolean);
    procedure SetCaseCode(l, c: Integer; const Value: Char);
    property CasesIsWall[l, c: Integer]: Boolean write SetCaseIsWall;
    property CasesWallUp[l, c: Integer]: Boolean write SetCaseWallUp;
    property CasesWallDown[l, c: Integer]: Boolean write SetCaseWallDown;
    property CasesWallLeft[l, c: Integer]: Boolean write SetCaseWallLeft;
    property CasesWallRight[l, c: Integer]: Boolean write SetCaseWallRight;
    property CasesCode[l, c: Integer]: Char write SetCaseCode;
  public
    constructor Create;
    destructor Destroy; override;
    procedure LoadFile(const aFile: string);
    property NbCol: Integer read FNbCol;
    property NbLin: Integer read FNbLin;
    property Cases[l, c: Integer]: TCase read GetCase;
  end;

implementation

{ TCase }

function TCase.CaseToString: string;

  function Iif(b: Boolean; const s1, s2: string): string;
  begin
    if (b) then
      Result := s1
    else
      Result := s2;
  end;

begin
  Result := Format('%s%s%s%s%s', [Iif(IsWall, '#', '_'), Iif(WallUp, 'U', 'u'),
    Iif(WallRight, 'R', 'r'), Iif(WallDown, 'D', 'd'),
    Iif(WallLeft, 'L', 'l')]);
end;

{ TMaze }

constructor TMaze.Create;
begin
  FNbLin := 0;
  FNbCol := 0;
end;

destructor TMaze.Destroy;
begin
  inherited;
end;

procedure TMaze.SetCaseIsWall(l, c: Integer; const Value: Boolean);
begin
  FCases[l * FNbCol + c].IsWall := Value;
end;

procedure TMaze.SetCaseWallDown(l, c: Integer; const Value: Boolean);
begin
  FCases[l * FNbCol + c].WallDown := Value;
end;

procedure TMaze.SetCaseWallLeft(l, c: Integer; const Value: Boolean);
begin
  FCases[l * FNbCol + c].WallLeft := Value;
end;

procedure TMaze.SetCaseWallRight(l, c: Integer; const Value: Boolean);
begin
  FCases[l * FNbCol + c].WallRight := Value;
end;

procedure TMaze.SetCaseWallUp(l, c: Integer; const Value: Boolean);
begin
  FCases[l * FNbCol + c].WallUp := Value;
end;

procedure TMaze.SetCaseCode(l, c: Integer; const Value: Char);
begin
  FCases[l * FNbCol + c].Code := Value;
end;

function TMaze.GetCase(l, c: Integer): TCase;
begin
  Result := FCases[l * FNbCol + c];
end;

procedure TMaze.LoadFile(const aFile: string);
var
  lst: TStringList;
  i: Integer;
  len: Integer;
begin
  if FileExists(aFile) then
  begin
    lst := TStringList.Create;
    try
      lst.LoadFromFile(aFile);

      for i := Pred(lst.Count) downto 0 do
      begin
        if (Trim(lst[i]) = '') then
          lst.Delete(i)
        else
        begin
          lst[i] := TrimRight(lst[i]);
          len := lst[i].Length;
          if (len > FNbCol) then
            FNbCol := len;
        end;
      end;

      FNbLin := lst.Count;

      if (FNbLin > 0) and (FNbCol > 0) then
        LoadMaze(lst)
      else
      begin
        FNbLin := 0;
        FNbCol := 0;
      end;
    finally
      lst.Free;
    end;
  end;
end;

procedure TMaze.LoadMaze(const lst: TStringList);
var
  l, c: Integer;
  Val: Byte;
begin
  SetLength(FCases, FNbLin * FNbCol);

  for l := 0 to Pred(FNbLin) do
    for c := 0 to Pred(FNbCol) do
      CasesIsWall[l, c] := lst[l][c + 1] = '#';

  for l := 0 to Pred(FNbLin) do
    for c := 0 to Pred(FNbCol) do
    begin
      CasesWallUp[l, c] := (l > 0) and (Cases[l - 1, c].IsWall);
      CasesWallRight[l, c] := (c < FNbCol - 1) and (Cases[l, c + 1].IsWall);
      CasesWallDown[l, c] := (l < FNbLin - 1) and (Cases[l + 1, c].IsWall);
      CasesWallLeft[l, c] := (c > 0) and (Cases[l, c - 1].IsWall);

      Val := 0;
      if (Cases[l, c].WallLeft) then
        Val := Val + 1;
      if (Cases[l, c].WallUp) then
        Val := Val + 2;
      if (Cases[l, c].WallRight) then
        Val := Val + 4;
      if (Cases[l, c].WallDown) then
        Val := Val + 8;

      CasesCode[l, c] := IntToHex(Val, 1)[1];
    end;
end;

end.
