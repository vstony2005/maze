unit UMazeReader;

interface

uses
  System.SysUtils, Classes;

type
  TCase = record
    IsWall: Boolean;
    WallUp: Boolean;
    WallRight: Boolean;
    WallDown: Boolean;
    WallLeft: Boolean;
    Code: Char;
    function CaseToString: string;
  end;

  TMazeReader = class
  strict private
  private
    FFileName: string;
    FNbLin: Integer;
    FNbCol: Integer;
    FCases: array of array of TCase;
    procedure SetFileName(const Value: string);
    procedure LoadFile(const aFile: string);
    procedure LoadMaze(const lst: TStringList);
    procedure Debug;
  public
    class procedure ReadFile(const name: string);
    constructor Create;
    destructor Destroy; override;
    property FileName: string read FFileName write SetFileName;
    property NbCol: Integer read FNbCol;
    property NbLin: Integer read FNbLin;
  end;

implementation

{ TCase }

function TCase.CaseToString: string;

  function Iif(b: Boolean; const s1, s2: string): string;
  begin
    if (b) then
      Result := s1
    else
      Result := s2;
  end;

begin
  Result := Format('%s%s%s%s%s_%s', [Iif(IsWall, '#', '_'),
    Iif(WallUp, 'U', 'u'), Iif(WallRight, 'R', 'r'), Iif(WallDown, 'D', 'd'),
    Iif(WallLeft, 'L', 'l'), Code]);
end;

{ TMazeReader }

class procedure TMazeReader.ReadFile(const name: string);
var
  maze: TMazeReader;
begin
  maze := TMazeReader.Create;
  try
    maze.LoadFile(name);

    Writeln('end.');
    Readln;
  finally
    maze.Free;
  end;
end;

constructor TMazeReader.Create;
begin
  FNbLin := 0;
  FNbCol := 0;
  FFileName := '';
end;

procedure TMazeReader.Debug;
var
  l, c: Integer;
begin
  for l := 0 to Pred(FNbLin) do
  begin
    for c := 0 to Pred(FNbCol) do
      Write(FCases[l][c].CaseToString + ' - ');
    Writeln;
  end;
end;

destructor TMazeReader.Destroy;
begin
  inherited;
end;

procedure TMazeReader.LoadFile(const aFile: string);
var
  lst: TStringList;
  i: Integer;
  len: Integer;
begin
  FFileName := aFile;
  if FileExists(FFileName) then
  begin
    lst := TStringList.Create;
    try
      lst.LoadFromFile(FFileName);

      for i := Pred(lst.Count) downto 0 do
      begin
        if (Trim(lst[i]) = '') then
          lst.Delete(i)
        else
        begin
          lst[i] := TrimRight(lst[i]);
          len := lst[i].Length;
          if (len > FNbCol) then
            FNbCol := len;

          Writeln(lst[i]);
        end;
      end;
      Writeln;

      FNbLin := lst.Count;

      if (FNbLin > 0) and (FNbCol > 0) then
      begin
        LoadMaze(lst);
        Debug;
      end;
    finally
      lst.Free;
    end;
  end;
end;

procedure TMazeReader.LoadMaze(const lst: TStringList);
var
  l, c: Integer;
  val: Integer;
begin
  SetLength(FCases, FNbLin, FNbCol);

  for l := 0 to Pred(FNbLin) do
    for c := 0 to Pred(FNbCol) do
      FCases[l][c].IsWall := lst[l][c + 1] = '#';

  for l := 0 to Pred(FNbLin) do
    for c := 0 to Pred(FNbCol) do
    begin
      FCases[l][c].WallUp := (l > 0) and (FCases[l - 1][c].IsWall);
      FCases[l][c].WallRight := (c < FNbCol - 1) and (FCases[l][c + 1].IsWall);
      FCases[l][c].WallDown := (l < FNbLin - 1) and (FCases[l + 1][c].IsWall);
      FCases[l][c].WallLeft := (c > 0) and (FCases[l][c - 1].IsWall);

      val := 0;
      if (FCases[l][c].WallLeft) then
        val := val + 1;
      if (FCases[l][c].WallUp) then
        val := val + 2;
      if (FCases[l][c].WallRight) then
        val := val + 4;
      if (FCases[l][c].WallDown) then
        val := val + 8;

      FCases[l][c].Code := IntToHex(val, 1)[1];
    end;
end;

procedure TMazeReader.SetFileName(const Value: string);
begin
  FFileName := Value;
end;

end.
