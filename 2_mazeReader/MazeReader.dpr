program MazeReader;

{$APPTYPE CONSOLE}
{$R *.res}

uses
  System.SysUtils,
  UMazeReader in 'UMazeReader.pas';

begin
  try
    TMazeReader.ReadFile('C:\temp\maze.txt');
  except
    on E: Exception do
      Writeln(E.ClassName, ': ', E.Message);
  end;

end.
