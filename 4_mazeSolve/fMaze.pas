unit fMaze;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes,
  System.Variants,
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  FMX.Layouts, System.ImageList, FMX.ImgList, UMaze;

type
  TFrmMaze = class(TFrame)
    ScrollBox1: TScrollBox;
    imgs: TImageList;
  private
    { Déclarations privées }
    FImgMaze: TBitmap;
    function LoadImages: Boolean;
    function AddImage(aBitmap: TBitmap; const name: string): Integer;
    procedure DrawCase(bmp: TBitmap; const name: string; const pos: TRect);
    procedure DrawMaze;
    procedure DrawSolve(l, c: Integer; path: string);
    function GetImageSize: Integer;
    function GetName(const aCase: TCase; l, c: Integer): string;
    property ImageSize: Integer read GetImageSize;
  public
    { Déclarations publiques }

    procedure Init;
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  end;

implementation

{$R *.fmx}

uses
  FMX.MultiResBitmap, FMX.Objects, UAlstar;

{ TFrmMaze }

constructor TFrmMaze.Create(AOwner: TComponent);
begin
  inherited;

  FImgMaze := nil;
  if (LoadImages) then
    DrawMaze
end;

destructor TFrmMaze.Destroy;
begin
  if Assigned(FImgMaze) then
    FImgMaze.Free;

  inherited;
end;

procedure TFrmMaze.Init;
begin
end;

function TFrmMaze.GetImageSize: Integer;
const
  SIZE_IMG = 16;
begin
  Result := SIZE_IMG;
end;

function TFrmMaze.AddImage(aBitmap: TBitmap; const name: string): Integer;
const
  SCALE = 1;
var
  srcItm: TCustomSourceItem;
  bmpItm: TCustomBitmapItem;
  dest: TCustomDestinationItem;
  layer: TLayer;
begin
  Result := -1;
  if (aBitmap.Width = 0) or (aBitmap.Height = 0) then
    Exit;

  // add source bitmap
  srcItm := imgs.Source.Add;
  srcItm.MultiResBitmap.TransparentColor := TColorRec.Fuchsia;
  srcItm.MultiResBitmap.SizeKind := TSizeKind.Source;
  srcItm.MultiResBitmap.Width := Round(aBitmap.Width / SCALE);
  srcItm.MultiResBitmap.Height := Round(aBitmap.Height / SCALE);
  srcItm.name := name;
  bmpItm := srcItm.MultiResBitmap.ItemByScale(SCALE, True, True);

  if not Assigned(bmpItm) then
  begin
    bmpItm := srcItm.MultiResBitmap.Add;
    bmpItm.SCALE := SCALE;
  end;

  bmpItm.Bitmap.Assign(aBitmap);

  dest := imgs.Destination.Add;
  layer := dest.Layers.Add;
  layer.SourceRect.Rect := TRectF.Create(TPoint.Zero,
    srcItm.MultiResBitmap.Width, srcItm.MultiResBitmap.Height);
  layer.name := srcItm.name;

  Result := dest.Index;
end;

function TFrmMaze.LoadImages: Boolean;
const
  PACK_FILE = '..\..\..\imgs\nb2.png';
var
  i: Integer;
  pack: TBitmap;
  img: TBitmap;
  rec: TRect;
begin
  Result := False;
  if FileExists(PACK_FILE) then
  begin
    pack := TBitmap.Create;
    try
      pack.LoadFromFile(PACK_FILE);

      for i := 0 to 24 do
      begin
        rec := Rect(i * ImageSize, 0, i * ImageSize + ImageSize, ImageSize);
        img := TBitmap.Create(ImageSize, ImageSize);
        img.CopyFromBitmap(pack, rec, 0, 0);
        AddImage(img, i.ToString);
        img.Free;
      end;

      Result := True;
    finally
      pack.Free;
    end;
  end;
end;

procedure TFrmMaze.DrawMaze;
var
  aMaze: TMaze;
  l, c: Integer;

  rec: TRectangle;
  a: TAlstar;
begin
  aMaze := TMaze.Create;
  if Assigned(FImgMaze) then
    FImgMaze.Free;

  FImgMaze := TBitmap.Create;
  try
    aMaze.LoadFile('C:\temp\maze.txt');

    FImgMaze.SetSize(aMaze.NbCol * ImageSize, aMaze.NbLin * ImageSize);

    // draw the maze
    for l := 0 to aMaze.NbLin - 1 do
    begin
      for c := 0 to aMaze.NbCol - 1 do
      begin
        DrawCase(FImgMaze, GetName(aMaze.Cases[l, c], l, c),
          Rect(ImageSize * c, ImageSize * l, ImageSize * c + ImageSize,
          ImageSize * l + ImageSize));
      end;
    end;

    // draw start and finish
    DrawCase(FImgMaze, '23', Rect(ImageSize, ImageSize, ImageSize * 2,
      ImageSize * 2));
    DrawCase(FImgMaze, '22', Rect(ImageSize * (aMaze.NbCol - 2),
      ImageSize * (aMaze.NbLin - 2), ImageSize * (aMaze.NbCol - 2) + ImageSize,
      ImageSize * (aMaze.NbLin - 2) + ImageSize));

    // draw the way out
    a := TAlstar.Create(aMaze, Point(1, 1), Point(aMaze.NbCol - 2,
      aMaze.NbLin - 2));
    try
      DrawSolve(1, 1, a.CalculatePath);
    finally
      a.Free;
    end;

    rec := TRectangle.Create(Self);
    rec.Parent := ScrollBox1;
    rec.Position.X := 10;
    rec.Position.Y := 10;
    rec.Size.Height := aMaze.NbLin * ImageSize;
    rec.Size.Width := aMaze.NbCol * ImageSize;
    rec.Fill.Kind := TBrushKind.Bitmap;
    rec.Fill.Bitmap.WrapMode := TWrapMode.TileStretch;
    rec.Stroke.Kind := TBrushKind.None;
//    rec.Align := TAlignLayout.Client;

    rec.Fill.Bitmap.Bitmap.Assign(FImgMaze);
  finally
    aMaze.Free;
  end;
end;

procedure TFrmMaze.DrawCase(bmp: TBitmap; const name: string; const pos: TRect);
const
  SCALE = 1;
var
  bmpItm: TCustomBitmapItem;

  bmpTmp: TBitmap;

  Size: TSize;
  src: TRectF;
begin
  imgs.BitmapItemByName(name, bmpItm, Size);

  bmpTmp := bmpItm.Bitmap;
  src := TRectF.Create(0, 0, bmpTmp.Width, bmpTmp.Height);

  bmp.Canvas.BeginScene;
  bmp.Canvas.DrawBitmap(bmpTmp, src, pos, 1);
  bmp.Canvas.EndScene;
end;

procedure TFrmMaze.DrawSolve(l, c: Integer; path: string);
var
  cr: Char;
  posx, posy: Integer;
begin
  posx := c;
  posy := l;

  for cr in path do
  begin
    case cr of
      'l':
        posx := posx - 1;
      'u':
        posy := posy - 1;
      'r':
        posx := posx + 1;
      'd':
        posy := posy + 1;
    else
      Continue;
    end;

    DrawCase(FImgMaze, '20', Rect(posx * ImageSize, posy * ImageSize,
      posx * ImageSize + ImageSize, posy * ImageSize + ImageSize));
  end;
end;

function TFrmMaze.GetName(const aCase: TCase; l, c: Integer): string;
var
  idx: Integer;
begin
  idx := -1;
  try
    if not(aCase.IsWall) then
      idx := 16
    else
      idx := StrToInt('$' + aCase.Code);
  except
    ShowMessageFmt('%s [%s] (%d-%d)', [aCase.CaseToString, aCase.Code, l, c]);
  end;

  case idx of
    0: // empty
      Result := '0';
    1: // left
      Result := '1';
    2: // up
      Result := '4';
    3: // left+up
      Result := '9';
    4: // right
      Result := '2';
    5: // right+left
      Result := '5';
    6: // up+right
      Result := '8';
    7: // left+up+right
      Result := '13';
    8: // down
      Result := '3';
    9: // left+down
      Result := '7';
    10: // up+down
      Result := '6';
    11: // left+up+down
      Result := '14';
    12: // right+down
      Result := '10';
    13: // left+right+down
      Result := '11';
    14: // up+right+down
      Result := '12';
    15: // left+up+right+down
      Result := '15';
  else
    Result := '17';
  end;
end;

end.
