unit UAlstar;

interface

uses
  System.SysUtils, System.Types, Classes, UMaze,
  System.Generics.Collections;

type
  { Defines a node of the path used by the A* method }
  TCasePath = class
  private
    FStart: TPoint;      // Starting point
    FParent: TCasePath;  // Origin node of case
    FH: Integer;         // Distance to the finish point
    FF: Integer;         // Total cost (G + H)
    FG: Integer;         // Distance traveled since the beginning
    FX: Integer;         // X position of the point
    FY: Integer;         // Y position of the point

    procedure Update(const c: TCasePath);

    class var FFinish: TPoint;
  public
    constructor Create(pStart: TPoint; aParent: TCasePath);

    property G: Integer read FG write FG;
    property H: Integer read FH write FH;
    property F: Integer read FF write FF;
    property X: Integer read FX;
    property Y: Integer read FY;
  end;

  { Search for a path using the A* method }
  TAlstar = class(TObject)
  private
    FMaze: TMaze;
    FBegin: TPoint;
    FEnd: TPoint;
    FFind: Boolean;

    FOpened: TList<TCasePath>;
    FClosed: TList<TCasePath>;

    procedure CheckNode(cp: TCasePath; l, c: Integer);
    procedure FreeList(lst: TList<TCasePath>);
  public
    constructor Create(const maze: TMaze; pBegin, pEnd: TPoint);
    destructor Destroy; override;

    function CalculatePath: string;
    function PathToString(current: TCasePath): string;
  end;

implementation

uses
  Math, System.Generics.Defaults;

{ TAlstar }

constructor TAlstar.Create(const maze: TMaze; pBegin, pEnd: TPoint);
begin
  FMaze := maze;
  FBegin := pBegin;
  FEnd := pEnd;
  FFind := False;

  FOpened := TList<TCasePath>.Create;
  FClosed := TList<TCasePath>.Create;
end;

destructor TAlstar.Destroy;
begin
  FreeList(FOpened);
  FreeList(FClosed);

  FOpened.Free;
  FClosed.Free;

  inherited;
end;

procedure TAlstar.FreeList(lst: TList<TCasePath>);
var
  i: Integer;
begin
  for i := Pred(lst.Count) downto 0 do
    lst[i].Free;
  lst.Clear;
end;

function TAlstar.CalculatePath: string;
var
  current: TCasePath;
begin
  FreeList(FOpened);
  FreeList(FClosed);

  TCasePath.FFinish := FEnd;

  current := TCasePath.Create(FBegin, nil);
  FOpened.Add(current);
  FFind := False;

  while (FOpened.Count > 0) and (not FFind) do
  begin
    FOpened.Sort(TComparer<TCasePath>.Construct(
      function(const Left, Right: TCasePath): Integer
      begin
        Result := Right.F - Left.F;
      end));

    current := FOpened[0];
    FFind := (Abs(current.X - FEnd.X) + Abs(current.Y - FEnd.Y) <= 1);

    FClosed.Add(current);
    FOpened.Remove(current);

    if (not FFind) then
    begin
      CheckNode(current, current.Y, current.X + 1);
      CheckNode(current, current.Y, current.X - 1);
      CheckNode(current, current.Y + 1, current.X);
      CheckNode(current, current.Y - 1, current.X);

      current := nil;
    end
    else
      current := TCasePath.Create(FEnd, current);
  end;

  Result := PathToString(current);
  current.Free;
end;

procedure TAlstar.CheckNode(cp: TCasePath; l, c: Integer);

  function ListFind(lst: TList<TCasePath>): TCasePath;
  var
    cas: TCasePath;
  begin
    Result := nil;
    for cas in lst do
    begin
      if (cas.X = c) and (cas.Y = l) then
        Result := cas;
      if Assigned(Result) then
        Exit;
    end;
  end;

var
  ctrl: TCasePath;
begin
  if (l < 0) or (l >= FMaze.NbLin) or (c < 0) or (c >= FMaze.NbCol) then
    Exit;

  if (FMaze.Cases[l, c].IsWall) then
    Exit;

  ctrl := ListFind(FClosed);
  if Assigned(ctrl) then
    Exit;

  ctrl := ListFind(FOpened);
  if not Assigned(ctrl) then
    FOpened.Add(TCasePath.Create(Point(c, l), cp))
  else
    ctrl.Update(cp);
end;

function TAlstar.PathToString(current: TCasePath): string;
// Returns the list of movements to reach the finish
var
  cas: TCasePath;
  par: TCasePath;
  sb: TStringBuilder;
begin
  Result := string.Empty;
  if not Assigned(current) then
    Exit;

  cas := current;
  par := cas.FParent;

  sb := TStringBuilder.Create;

  while Assigned(par) do
  begin
    if (cas.X - par.X > 0) then
      sb.Insert(0, 'r')
    else if (cas.X - par.X < 0) then
      sb.Insert(0, 'l')
    else if (cas.Y - par.Y > 0) then
      sb.Insert(0, 'd')
    else if (cas.Y - par.Y < 0) then
      sb.Insert(0, 'u');

    cas := cas.FParent;
    par := cas.FParent;
  end;

  Result := sb.ToString;
  sb.Free;
end;

{ TCasePath }

constructor TCasePath.Create(pStart: TPoint; aParent: TCasePath);
begin
  FStart := pStart;
  FParent := aParent;

  if not Assigned(FParent) then
    FH := 0
  else
    FG := FParent.G + 1;

  H := Abs(FStart.X - FFinish.X) + Abs(FStart.Y - FFinish.Y);
  F := G + H;

  FX := FStart.X;
  FY := FStart.Y;
end;

procedure TCasePath.Update(const c: TCasePath);
// Update, if necessary, the information in the case
var
  done: Integer;
  stay: Integer;
begin
  done := c.G + 1;
  stay := Abs(X - FStart.X) + Abs(Y - FStart.Y);

  if (F > done + stay) then
  begin
    FParent := c;
    G := done;
    H := stay;
    F := G + H;
  end;
end;

end.
