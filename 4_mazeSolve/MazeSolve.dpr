program MazeSolve;

uses
  madExcept,
  madLinkDisAsm,
  madListHardware,
  madListProcesses,
  madListModules,
  System.StartUpCopy,
  FMX.Forms,
  Main in 'Main.pas' {FMain1},
  UMaze in 'UMaze.pas',
  fMaze in 'fMaze.pas' {FrmMaze: TFrame},
  UAlstar in 'UAlstar.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TFMain1, FMain1);
  Application.Run;
end.
